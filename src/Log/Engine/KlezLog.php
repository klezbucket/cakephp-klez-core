<?php


namespace KlezCore\Log\Engine;


use Cake\Log\Engine\FileLog;

/**
 * The KlezCore log adapter.
 *
 * Based in the FileLog, it prints organized output into the file logs.
 *
 * Class KlezLog
 * @see FileLog
 * @package KlezCore\Log\Engine
 */
class KlezLog extends FileLog {
    /**
     * @var array The settings for this log adapter.
     */
    private $options = [];

    /**
     * @var string A string to prefix the output, unique for every instance of the log adapter.
     */
    private $hash = null;

    /**
     * KlezLog constructor.
     *
     * Ensure some entries are in sane defaults.
     * Path defaults to LOGS
     * Scopes defaults to all scopes
     * Enabled defaults to false
     *
     * @param array $options
     */
    public function __construct($options = []){
        $options['path'] = $options['path'] ?? LOGS;
        $options['scopes'] = $options['scopes'] ?? [];
        $options['enabled'] = $options['enabled'] ?? false;
        $this->options = $options;
        parent::__construct($options);
    }

    /**
     * Logs with an arbitrary level.
     *
     * Uses the scope specified in the options.
     * The raw message is concatenated with a unique per instance hash.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    public function log($level, $message, array $context = array()){
        if($this->options['enabled']){
            parent::log($level,$this->getFullMessage($message),$this->options['scopes'] ?? []);
        }
    }

    /**
     * @param $message The raw message
     * @return string The concatenation between hash and raw message
     */
    private function getFullMessage($message){
        $hash = $this->generateHash();

        if(isset($this->options['maxlength'])){
            $message = substr($message,0,(int) $this->options['maxlength']);
        }

        return $hash . ' | ' . $message;
    }

    /**
     * @return string The unique hash generated for this log adapter instance.
     */
    private function generateHash(){
        if(is_null($this->hash)){
            $this->hash = substr(md5(uniqid()),0,8);
        }

        return $this->hash;
    }
}