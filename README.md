# KlezCore plugin for CakePHP

Welcome to the KlezPlugin's family.

We aim to automatize and make easy the repetitive parts of any project. Extending the framework to provide the hooks and settings to do so.

## Changelog v0.2-beta

This is the second commit. 

This package is the common ground to all the KlezPlugin's family. So expect us.

##### KlezLog
The KlezLog Log Adapter has been created as described by the [cookbook](https://book.cakephp.org/3.0/en/core-libraries/logging.html#creating-log-adapters)

Example config:
```
        Log::setConfig('klezcore', [
            'className' => 'KlezCore.Klez',
            'enabled' => true,
            'scopes' => [
                'klezcore'
            ]
        ]);
```

Usage:

```
        Log::write('info','Hello!',['klezcore']);
```

## Changelog v0.1-beta

This is the first commit. 

This package is the common ground to all the KlezPlugin's family. So expect us.

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require klezbucket/cakephp-klez-core
```
